#!/usr/bin/perl
#this script tries to extract proteins homologous to a probe sequence using tBLASTn so that it can detect sequences with frameshift mutations and small insertions or deletions. It was written in the context of Matthew Turnbull's Master's Thesis project in the Douville Lab at the University of Winnipeg
#Author: Matthew Turnbull, University of Winnipeg
=pod
=head1 Notes
This program was designed to do two things: 1) use tBLASTx to search a local database using a protein sequence as a probe and 2) process the hits to try and eliminate non-homologous sequences and merge duplicate hits.

It takes as input a blast database and a probe sequence, in the future maybe I'll un-hardcode some settings, in the meanwhile feel free to modify the code yourself. It depends on BLAST+ for obvious reasons, and it also depends on BioPerl and MACSE, so therefor java.

A lot of the code is poorly commented, and it will be refactored in the future in case someone actually wants to use it. If you do, let me know if you have any trouble at ave.jor@gmail.com
Also, I have plans to update this code, although I will be keeping this version of it archived also since it's the one that goes with my thesis.

=cut

#globals etc
use warnings;
use Bio::SearchIO;
use Data::Dumper;
use Bio::SeqFeature::Generic;
use Bio::Seq;
use Bio::SeqIO;
use Bio::Align::AlignI;
use Bio::AlignIO;
#use strict;
my $test=0;
my $run_time=time();
my $lastname = "";
my @annotatedblastdatabasesequences=();
#$gappedmacsemsa;
#BLAST+ path if it's not in your PATH
my $blastpluspath=`which tblastn` or $blastpluspath="C:/Program Files/Unipro UGENE/tools/blast+-2.2.28/bin";
my $makeblastdb=`which makeblastdb` or $makeblastdb="";
chomp $blastpluspath;
chomp $makeblastdb;
#set up the MACSE path or else die
my $macse = "/home/matthew/ProteaseProject/software/MACSE/macse_v1.01b.jar";
if($0 =~ /(.+)\/bin\/.+\.plx/){
    $macse = "$1/software/MACSE/macse_v1.01b.jar";
}else{
    print "#MACSE path set automatically\n";
}
my @callout;
#get command line args
die "usage: perl BLASTForProteases.plx \$blastdirectory \$probesequence.fasta" unless $#ARGV == 1;
my $blastdir = shift;
my $blastseq = shift;
print "starting $0\n";
opendir BLASTDIR, $blastdir;
#find all the files to search, originally this only looked at files from GRCh38, which is why it looks for chr in the name
#@chromosomefiles=grep(/chr([\dXYMT]+|unlocalized|unplaced)\.fa$/,readdir(BLASTDIR));
@chromosomefiles=grep(/\.fa$/,readdir(BLASTDIR));
print @chromosomefiles;
foreach (@chromosomefiles){
$sequencename = $1 if /(.+)\.fa$/;
&BLAST;
next unless -f $outfile;
&PROCESS;
&EXTRACTSEQUENCES;
&EXTEND;
undef @thesehitsonchromosome;
undef @processedblasthits;
undef @extendedout;
}
exit();

sub BLAST{
    print "#running BLAST\n";
#make database
#version for windows
#@callout=($blastpluspath."/makeblastdb.exe", "-in", "C:/Sequences/$sequencename.fa", "-dbtype", "nucl", "-parse_seqids", "-out", "C:/ProjectLarge/$sequencename");
#system(@callout) unless -e "C:/ProjectLarge/$sequencename.nsq";
    @callout = ($makeblastdb, "-in", "${blastdir}/${sequencename}.fa", "-dbtype", "nucl", "-parse_seqids", "-out", "${blastdir}/${sequencename}");
    system(@callout) unless -e "${blastdir}/${sequencename}.nsq";
#run BLAST
#@callout=($blastpluspath."/tblastn.exe", "-version");
    @callout=($blastpluspath, "-version");
    system(@callout);
#should I use -html?
$outfile = "${sequencename}-${run_time}-output";
$outfile = "meowfiles" if $test;
#my @callout=("$blastpluspath/tblastn.exe", "-query", $_[1], "-db", "C:/ProjectLarge/$sequencename", "-out", $outfile, "-evalue", 0.1, "-matrix", "PAM30", "-word_size", 3, "-window_size", 0, "-max_intron_length", 99, "-outfmt", 5);
my @callout=("$blastpluspath", "-query", $blastseq, "-db", "${blastdir}/${sequencename}", "-out", $outfile, "-evalue", 0.1, "-matrix", "PAM30", "-word_size", 3, "-window_size", 0, "-max_intron_length", 99, "-outfmt", 5);
system(@callout) unless -f $outfile;
}
#process BLAST results
sub PROCESS{
    print "#processing BLAST results\n";
$blastresults = new Bio::SearchIO(-format => "blastxml", -file => "$outfile");
    while ( my $result = $blastresults->next_result){
        my @hits = $result->hits;
        for $hit ( sort { $a->start('hit') <=> $b->start('hit') } @hits){
            @hsps = $hit->hsps;
            for $hsp ( sort { $a->start('hit') <=> $b->start('hit') } @hsps){
            ## $hit is a Bio::Search::Hit::HitI compliant object
            print "Query=",   $result->query_name,
                " Hit=",        $hit->name,
                " Start=",      $hsp->start('hit'),
                " End=",        $hsp->end('hit'),
                " Length=",     $hsp->length('total'),
                " Percent_id=", $hsp->percent_identity, "\n";
            my $processedhit = new Bio::SeqFeature::Generic(-start => $hsp->start('hit'), -end=> $hsp->end('hit'), -strand=> $hsp->strand('hit'), -source_tag=> 'blast', -primary_tag=>$hit->name);
            push @processedblasthits, $processedhit;
            unless($processedblasthits[-2]){
                print "no previous hit\n";
                next;
            }
            unless($result->query_name eq $lastname){
                print "same hit\n";
                my $lastname = $result->query_name;
#                next;
            }
            if( $hsp->start('hit')-$processedblasthits[-2]->end < $processedblasthits[-2]->length and $processedblasthits[-2]->primary_tag eq $processedblasthits[-1]->primary_tag)
                {
                $processedblasthits[-2]->end($processedblasthits[-1]->end);
                print "merged!\n";
                pop @processedblasthits;
                }else{
                    print "not merged\n";
                }
            }
        }
    }
foreach(@processedblasthits){print $_->start," ",$_->end,"\n"}
}
#get sequences
sub EXTRACTSEQUENCES{
    print "#extracting sequences from the blast database\n";
    $chromosome = Bio::SeqIO->new(-file=> "<${blastdir}/${sequencename}.fa") or die "${sequencename} didn't exist!\n";
    $hitsequences = Bio::SeqIO->new(-file=> ">$outfile"."blastsequences.gb", -format=>"genbank");
	$hitsequencesformacse = Bio::SeqIO->new(-file=> ">$outfile"."ForMacse.fasta", -format=>"fasta");
    while( my $thischromosome = $chromosome->next_seq){
        print "processing ", $thischromosome->primary_id, "\n";
        unshift @annotatedblastdatabasesequences, $thischromosome;
    $thischromosome->add_SeqFeature(@processedblasthits) or die "they didn't add\n";
    foreach $eachmergedhit ( $thischromosome->get_SeqFeatures )
    {
        if($eachmergedhit->primary_tag eq $thischromosome->primary_id){
            print $eachmergedhit->primary_tag, "\t", $thischromosome->primary_id, "\n";
            print $eachmergedhit->seq->translate->seq . "\n";
        $thesehitsonchromosome{$thischromosome->primary_id . "/" . $eachmergedhit->start} = Bio::SeqFeature::Generic->new(-start => $eachmergedhit->start, -end => $eachmergedhit->end, -strand => $eachmergedhit->strand, -primary => $eachmergedhit->primary_tag);
        $annotatedblastdatabasesequences[0]->add_SeqFeature($thesehitsonchromosome{$thischromosome->primary_id . "/" . $eachmergedhit->start});
		push @thesehitsonchromosome, Bio::Seq->new(-seq=>$eachmergedhit->seq->seq, -id=>$thischromosome->primary_id . "/" . $eachmergedhit->start);
        }else{
            print "Not a match\n";
        }
    }
	&MACSE($outfile."ForMacse") if $test;
    }
	$hitsequences->write_seq(@thesehitsonchromosome);
	$hitsequencesformacse->write_seq(@thesehitsonchromosome);
	&MACSE($outfile."ForMacse");
    foreach $test (keys %thesehitsonchromosome){
        print "$test\t", $thesehitsonchromosome{$test}->seq->translate->seq, "\n";
    }
}
#run macse
sub MACSE
{
    print "#running MACSE\n";
my $macsefilename=$_[0];
$macseinfile=$macsefilename.".fasta";
$macseoutfile=$macsefilename."_macse_NT.fasta";
$gappedmacsemsa=$macsefilename."_macse_NTGaps.fasta";
@callout=("java", "-jar", "$macse", "-prog", "alignSequences", "-seq", $macseinfile, "-");
print @callout;
    unless( -e $macseoutfile){
        system(@callout);
    }else{
        print "skipped MACSE because ${macseoutputfile} existed.\n"
    }
open FILE, '<', $macseoutfile or die "couldn't open $macseoutfile";
if($macseoutfile=~/^\.[\\|\/]/){$macseoutfile=~s/\.[\\|\/]//;}
open OUTPUT, '>', $gappedmacsemsa or die "couldn't open output file.";
$i=0;
while(<FILE>){$i += s/[!|*|?]/-/g;print OUTPUT $_;}
print "$i characters in $macseoutfile replaced with '-'.\nOutput written to ",$gappedmacsemsa,"\n";
close OUTPUT;
close FILE;
}
#extend processed alignment
sub EXTEND
{
    print "#extending alignment\n";
#load msa
print $gappedmacsemsa,"\n";
$aln = Bio::AlignIO->new(-file => $gappedmacsemsa, -format => 'fasta') or die "couldn't get alignments";
	$extendedsequencesout = Bio::SeqIO->new(-file=> ">$outfile"."FullSequences.fasta", -format=>"fasta");
	$extendedsequencesoutgb = Bio::SeqIO->new(-file=>">$outfile"."FullSequences.gb",-format=>"genbank");
foreach $inputalignment ($aln->next_aln){
  print 'name : ', $inputalignment->id, "\n";
  print 'width : ', $inputalignment->length, "\n";
  print 'size : ', $inputalignment->num_residues, "\n";
  if($inputalignment->is_flush){print "It's flush.\n";}else{print "It's not flush.\n";};
  print 'number of lines : ', $inputalignment->num_sequences, "\n";
  print 'identity: ', $inputalignment->percentage_identity, "\n";

foreach $msaline ($inputalignment->each_seq){
#'Name\tLength\tStartPostion\tEndPosition'
    my $printstart=0;
    my $printend=0;
    print $thesehitsonchromosome{$msaline->id}->start, "\t";
	print $out=$msaline->id, "\t";
	$seq = $msaline->seq;
	$seq=~s/\-//g;
	print $length= length($seq), "\t";
	print $first = $inputalignment->column_from_residue_number($out,1), "\t";
	print $last = $inputalignment->column_from_residue_number($out,$length), "\n";
    print $thesehitsonchromosome{$msaline->id}->seq->translate->seq, "\n";
	if($thesehitsonchromosome{$msaline->id}->strand > 0)
	{
		my $newstart=$thesehitsonchromosome{$msaline->id}->start-$first+1;
		my $newend=$thesehitsonchromosome{$msaline->id}->end+$inputalignment->length-$last;
		$thesehitsonchromosome{$msaline->id}->start($newstart);
		$thesehitsonchromosome{$msaline->id}->end($newend);
        $printstart=$newstart;
        $printend=$newend;
	}elsif($thesehitsonchromosome{$msaline->id}->strand < 0)
	{
		my $newstart=$thesehitsonchromosome{$msaline->id}->start-$inputalignment->length+$last;
		my $newend=$thesehitsonchromosome{$msaline->id}->end+$first-1;
		$thesehitsonchromosome{$msaline->id}->start($newstart);
		$thesehitsonchromosome{$msaline->id}->end($newend);
        $printstart=$newstart;
        $printend=$newend;
	}else
	{
	print $msaline->id, " had strand ", $thesehitsonchromosome{$msaline->id}->get_SeqFeatures('strand')," for some reason \n";
	}
	print $thesehitsonchromosome{$msaline->id}->start, "\t";
	$seq = $thesehitsonchromosome{$msaline->id}->seq->seq;
	$seq=~s/\-//g;
	print length($seq), "\t$printstart\t$printend\n";
    print $thesehitsonchromosome{$msaline->id}->seq->translate->seq . "\n";
    push @extendedout, Bio::Seq->new(-seq=>$seq, -id=>$msaline->id . "/" . $thesehitsonchromosome{$msaline->id}->start);
}
    $extendedsequencesout->write_seq(@extendedout);
	$extendedsequencesoutgb->write_seq(@extendedout);
	&MACSE($outfile."FullSequences");
	}
}
