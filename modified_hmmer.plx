#!/usr/bin/perl -w
#this script runs hmmer on the possible translations of a sequence
#modules
#changelog
#Fri Nov 13 16:01:17 CST 2015
#the program didn't indicate which frame translations were from, which wasn't super useful. I change it to do that.
#Fri Nov 13 21:00:15 CST 2015
#I've also made it able to merge adjacent hits if they are from non-overlaping parts of the hmm!
#Sat Nov 14 00:01:39 CST 2015
#I've figured out that the positional information provided by the hmm hits is relative to the translational position, not the nucleotide, so it needs to be transformed. Starts should be "start*3 - 2" I think, and ends should be end *3
use Bio::SeqIO;
use Bio::Seq;
use Bio::SimpleAlign;
use Bio::AlignIO;
use Data::Dumper;
use strict;
#variables
my $seqdb;
my $hmmdb;
my $tblout;
my $hmmout;
my %sequence_list;
my %first_and_last_gap;
my $last_id;
my $last_gap;
my $first_gap;
my $verbose = 0;
#used to keep track of domain length
my $this_length = 0;
my $max_length = 0;

#usage
my $usage="perl<script> <sequence database> <phmms>\n\t<sequence database>\ta fasta flat-file containing the nucleotide sequences to be searched using the hmm\n\t<phmms>\tthese are the hmms which will be searched in the database. They have to be protein hmms.";

&load;
&checkstuff;
&translate_it;
&merge;
&cleanup;

sub checkstuff{
    #this subroutine checks that everything is OK
    my $hmmerpath = `which hmmsearch` or die("hmmsearch appears to not be installed.\n");
    print "Using $hmmerpath\n" if $verbose;
}

sub load{
    print "loading...\n";
    if($ARGV[0] eq "-verbose"){
        $verbose++;
        shift @ARGV;
    }
    $seqdb = shift @ARGV or die $usage;
    $hmmdb = shift @ARGV or die $usage;
    $hmmout = $seqdb.".hmmer_$1" if $hmmdb =~ /([^\/]+$)/;
    $tblout = $hmmout.".tblout";
}

sub translate_it{
    print "translating...\n";
    my $seqdb_in = Bio::SeqIO->new(-file => "$seqdb", -format => 'fasta');
    my $seqdb_out = Bio::SeqIO->new(-file => ">hmm_translation_temp.fasta", -format => 'fasta');
    while(my $new_seq = $seqdb_in->next_seq){
        my $id = $new_seq->display_id;
        my $frame_zero = $new_seq->translate(-frame => 0);
        $frame_zero->display_id("$id.zero");
        my $frame_one = $new_seq->translate(-frame => 1);
        $frame_one->display_id("$id.one");
        my $frame_two = $new_seq->translate(-frame => 2);
        $frame_two->display_id("$id.two");
        $seqdb_out->write_seq($frame_zero);
        $seqdb_out->write_seq($frame_one);
        $seqdb_out->write_seq($frame_two);
        }
    print "searching...\n";
    my @hmmer=("hmmsearch","--incE","1.0","--incdomE","1.0","-A","$hmmout.ali","-o","$hmmout","--tblout","$tblout",$hmmdb,"hmm_translation_temp.fasta");
    system @hmmer;
    }

sub merge{
#reset the sequence data base, now we're looking at the results of the original search to see if they match and we intend on merging them.
#make sure the file is not empty
    die "The hmm search results were empty.\n" unless -f "$hmmout.ali";
    my $hmmdb_in = Bio::AlignIO->new( -file => "$hmmout.ali",
                                 -format => "stockholm" );
#    my $hmmdb_out = Bio::AlignIO->new( -file => ">$hmmout.merged",
#                                  -format => "fasta" );
#this is where we will put the merged nucleotide data
    my $seqdb_out = Bio::SeqIO->new(-file => ">$hmmout.nuc", -format => 'fasta');
#we will need the nucleotide data later to merge hits
    my $seqdb_in = Bio::SeqIO->new(-file => "$seqdb", -format => 'fasta');
#we need to load the sequences into memory if they're short enough, because it take a while otherwise.
    my %nucleotide_data;
    while( my $nucleotide_sequence = $seqdb_in->next_seq){
        $nucleotide_data{$nucleotide_sequence->display_id} = $nucleotide_sequence;
    }
    print Dumper %nucleotide_data if $verbose;
#get the next alignment (probably the only alignment)
    my $aln = $hmmdb_in->next_aln;
    print Dumper $aln if $verbose;
    print "This alignment is " , $aln->length , " wide.\n";
    foreach my $seq ($aln->each_seq){
#get information on the sequences. It looks like the start and end actually correspond to the nucleotide sequences that was searched earlier, which is pretty sweet.
        print $seq->display_id , " " , $seq->start , " " , $seq->end , "\n";
#assemble a list of sequences from the same sequence
        push @{$sequence_list{$1}}, $seq->display_id if $seq->display_id =~ /^(.+)\.(zero|one|two)$/;
    }
#for each sequence that could be merged, check to make sure they don't overlap to much or that they aren't too  far apart.
    foreach my $potential_merger (keys %sequence_list){
        unless( scalar @{$sequence_list{$potential_merger}} > 1 ){
#since this sequence won't be merged, output it now
            my $temp_nuc_seq = $nucleotide_data{$potential_merger}->subseq($aln->get_seq_by_id($sequence_list{$potential_merger}[0])->start * 3 - 2 => $aln->get_seq_by_id($sequence_list{$potential_merger}[0])->end * 3);
            $nucleotide_data{$potential_merger}->seq($temp_nuc_seq);
            $seqdb_out->write_seq($nucleotide_data{$potential_merger});
#then skip trying to merge it
        next;
        }
        foreach(@{$sequence_list{$potential_merger}}){
#            my $first_position = $aln->column_from_residue_number($_,1);
#            print "$_\'s first position is at $first_position.\n";
            print "ID $_:\n" if $verbose;
            print $aln->get_seq_by_id($_)->seq, "\n" if $verbose;
            $this_length = length $aln->get_seq_by_id($_)->seq;
            $max_length = $this_length if $this_length > $max_length;
#determine where the first non-gap character in the sequences is, and also the last non-gap character
#these will be stored in %first_and_last_gap
            if($aln->get_seq_by_id($_)->seq =~ /^([\-\.]+)/){
                $first_gap = length $1;
            }else{
                $first_gap = 0;
            }
            if($aln->get_seq_by_id($_)->seq =~ /([\-\.]+)$/){
                $last_gap = length $1;
            }else{
                $last_gap = 0;
            }
#this is first non gap, last non gap, position of first non gap, position of last non gap
            @{$first_and_last_gap{$_}} = ($first_gap, $last_gap, $first_gap + 1, $aln->length - $last_gap );
        }
#run through the sequences again, but this time check if we can merge them.
        foreach(@{$sequence_list{$potential_merger}}){
            if($last_id){
#if the end of the current sequence is earlier than the start of the last one we looked at
                print "comparing $last_id and $_\n";
                print ${$first_and_last_gap{$last_id}}[2], " until ", ${$first_and_last_gap{$last_id}}[3], " and ", ${$first_and_last_gap{$_}}[2], " until ", ${$first_and_last_gap{$_}}[3], "\n";
#the criteria here is that the hits are less than half a domain apart, wherea domain is the length of the longest sequence.
#they must also not overlap by more than half a domain length
                if( abs(${$first_and_last_gap{$_}}[3] - ${$first_and_last_gap{$last_id}}[2]) < ($max_length/2) and (${$first_and_last_gap{$_}}[2] < ${$first_and_last_gap{$last_id}}[2]) ){
                    print "ok, it thinks we should merge from ", $aln->get_seq_by_id($_)->start, " in $_ until " , $aln->get_seq_by_id($last_id)->end, " in $last_id.\n";
                    my $temp_nuc_seq = $nucleotide_data{$potential_merger}->subseq($aln->get_seq_by_id($_)->start * 3 - 2 => $aln->get_seq_by_id($last_id)->end * 3 );
                    $nucleotide_data{$potential_merger}->seq($temp_nuc_seq);
                    $seqdb_out->write_seq($nucleotide_data{$potential_merger});

                }elsif( abs(${$first_and_last_gap{$last_id}}[3] - ${$first_and_last_gap{$_}}[2]) < ($max_length/2) and (${$first_and_last_gap{$last_id}}[2] < ${$first_and_last_gap{$_}}[2]) ){
                    print "ok, it thinks we should merge from ", $aln->get_seq_by_id($last_id)->start, " in $last_id until " , $aln->get_seq_by_id($_)->end, " in $_.\n";
                    my $temp_nuc_seq = $nucleotide_data{$potential_merger}->subseq($aln->get_seq_by_id($last_id)->start * 3 - 2 => $aln->get_seq_by_id($_)->end * 3 );
                    $nucleotide_data{$potential_merger}->seq($temp_nuc_seq);
                    $seqdb_out->write_seq($nucleotide_data{$potential_merger});
                }
            }
#rember the last one for next time
            $last_id = $_;
        }
        undef $last_id;
    }
}

sub cleanup{
    system qw(unlink hmm_translation_temp.fasta);
}
